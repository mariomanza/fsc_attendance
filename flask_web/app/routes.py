import flask
from flask import request, jsonify
from app import app, db

@app.route('/login', methods=['POST'])
def login():
    data = []
    body = request.get_json(force=True)
    data = db.login(body['usr'],body['psw'])
    return jsonify(data), 200

@app.route('/register', methods=['POST'])
def register():
    data = [True]
    body = request.get_json(force=True)
    db.register(body['usr'],body['psw'])
    return jsonify(data), 200

@app.route('/admins', methods=['GET'])
def admins():
    data = []
    body = request.get_json(force=True)
    if "id" in body:
        data = db.get_admins({'id': body['id']})
    elif "nam" in body:
        data = db.get_admins({'name': body['name']})
    else:
        data = db.get_admins({'limit': body['limit'], 'offset': body['offset']})
    return jsonify(data), 200

@app.route('/users', methods=['GET'])
def users():
    data = []
    body = request.get_json(force=True)
    if "id" in body:
        data = db.get_users({'id': body['id']})
    elif "nam" in body:
        data = db.get_users({'name': body['name']})
    else:
        data = db.get_users({'limit': body['limit'], 'offset': body['offset']})
    return jsonify(data), 200

@app.route('/records', methods=['GET'])
def records():
    data = []
    body = request.get_json(force=True)
    data = db.get_records({'limit': body['limit'], 'offset': body['offset']})
    return jsonify(data), 200

@app.route('/reports')
def reports():
    data = []
    body = request.get_json()
    return jsonify(data), 200

@app.route('/qrcode')
def qrcode():
    data = []
    body = request.get_json()
    return jsonify(data), 200