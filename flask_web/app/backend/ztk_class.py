import zk
import pickle
import datetime

class Ztk:
    def __init__(self):
        self.ip = '172.16.2.197'
        self.port = 4370
        self.conn = zk.ZK(self.ip, self.port).connect()
    
    def get_info(self):
        return {
            "Firmware":self.conn.get_firmware_version(), "Serial":self.conn.get_serialnumber(),
            "Platform":self.conn.get_platform(), "Name":self.conn.get_device_name(),
            "Network":self.conn.get_network_params(), "MAC":self.conn.get_mac(),
        }
    
    def get_records(self):
        attend = self.conn.get_attendance()
        records = []
        for i in attend:
            records.append({i.user_id: {"date":i.timestamp.strftime("%Y-%m-%d"), "time":i.timestamp.strftime("%H:%M:%S"), "punch":i.punch, "status":i.status}})
        return records
    
    def get_users(self):
        users = self.conn.get_users()
        fingers = self.conn.get_templates()
        data = {}
        for u in users:
            data[u.uid] = {"user":pickle.dumps(u), "fingers":{}}
            for f in fingers:
                if u.uid == f.uid:
                    data[u.uid]["fingers"][str(f.fid)] = pickle.dumps(f)
        return data
    
    def upload_users(self, users):
        for data in users:
            user = pickle.loads(data['user'])
            fingers = [pickle.loads(value) for key, value in data['fingers'].items()]
            self.conn.set_user(uid=user.uid, name=user.name, user_id=user.user_id)
            self.conn.save_user_template(user, fingers)
        return "Loaded ..."
            

    def clear_records(self):
        self.conn.clear_attendance()
    
    def maintenace(self):
        self.conn.free_data()
        self.conn.restart()
        self.get_records()
        return "Maintenace Complete ..."

    def check(self):
        report = ""
        sys_time = self.conn.get_time()
        my_time = datetime.datetime.now()
        if sys_time == my_time:
            report = "All is Well ..."
        else:
            report = "Time is off by: {}:{} ...".format(*divmod((sys_time-my_time).seconds,60))
        return report

    def exit(self):
        self.conn.disconnect()

if __name__ == "__main__":
    pass