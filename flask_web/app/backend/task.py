from ztk_class import Ztk
from mongo_class import Mongod
import datetime

if __name__ == "__main__":
    log = open('logs/logs.txt', 'a')
    log_txt = ""
    try:
        log_txt += 'Creating ztk_obj ...\n'
        ztk = Ztk()
        log_txt += 'Extracting Records ...\n'
        records = ztk.get_records()
        log_txt += 'Records Extracted ...\nEnding ztk_obj ...\n'
        ztk.exit()
        del ztk

        log_txt += 'Creating mongod_obj ...\n'
        mdb = Mongod("mario", "12345")
        log_txt += 'Saving Data ...\n'
        mdb.store_records(records)
        log_txt += 'Data Saved ...\nEnding mongod_obj ...\nFinished ...'

    except Exception as e:
        error = open('logs/errors.txt', 'a')
        error.write("Date: {} \nProcess Error : {}\n\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),e))
        error.close()
    finally:
        log.write("Date: {} \nProcess Report : {}\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), log_txt))
        log.close()