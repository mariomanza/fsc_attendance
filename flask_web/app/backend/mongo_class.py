import pymongo
from bson.json_util import loads, dumps
import bcrypt

class Mongod:
    def __init__(self, usr, pwd):
        self.usr = usr
        self.pwd = pwd
        self.conn = pymongo.MongoClient('localhost', 27017)
        self.db = self.conn["fsc"]
    
    def login(self, usr, psw):
        col = self.db["admins"]
        data = col.find_one({"email":usr, "password":psw})
        if data is None:
            return False
        else:
            return dumps(data)

    def register(self, usr, psw):
        col = self.db["admins"]
        col.insert_one({"email":usr, "password":psw})

    def get_users(self, data):
        col = self.db["users"]
        if 'limit' in data:
            users = []
            for u in col.find().skip(data['offset']).limit(data['limit']):
                users.append(u)
            return users
        else:
            if 'id' in data:
                user = col.find({"_id": data['id']})
            if 'name' in data:
                user = col.find({"nam": data['name']})
        return dumps(user)
    
    def get_admins(self, data):
        col = self.db["admins"]
        if 'limit' in data:
            users = []
            for u in col.find().skip(data['offset']).limit(data['limit']):
                users.append(dumps(u))
            return users
        else:
            if 'id' in data:
                user = col.find({"_id": data['id']})
            if 'name' in data:
                user = col.find({"email": data['name']})
        return dumps(user)

    def backup_users(self, users):
        col = self.db["backup_users"]
        for id, data in users.items():
            if col.find_one({"_id":id}) is None:
                col.insert_one({"_id":id,"user":data['user'], "fingers":data['fingers']})
            else:
                col.update_one({"_id":id}, {"$set": {"user":data['user'], "fingers":data['fingers']}})
    
    def restore_users(self):
        col = self.db["backup_users"]
        users = []
        cur = col.find({})
        for doc in cur:
            users.append(doc)
        return users
    
    def store_users(self, users):
        col = self.db["users"]
        data = []
        for key, value in users.items():
            data.append({"_id":key, "data":value})
        col.insert_many(data)

    def store_records(self, records):
        col = self.db["records"]
        for i in records:
            for id, d in i.items():
                dt = d['date'].split('-')
                if col.find_one({"_id":id}) is None:
                    col.insert_one({"_id":id})
                col.update_one({"_id":id},{"$addToSet": {"{}.{}.{}".format(dt[0],dt[1],dt[2]):
                        {"time":d['time'],"status":str(d['punch'])}}})
    
    def get_records(self, data):
        col = self.db["records"]
        records = []
        for r in col.find().skip(data['offset']).limit(data['limit']):
            records.append(dumps(list(r.items())))
        return records

    def get_bydate(self, d):
        col = self.db["records"]
        records = []
        for r in col.find({"{}.{}.{}".format(d[0],d[1],d[2]): {"$exists":1}}):
            records.append(r)
        return records

    def get_byuser(self, id):
        col = self.db["records"]
        return col.find_one({"_id":id})

    def exit(self):
        self.conn.close()


if __name__ == "__main__":
    import json
    f = json.loads(open("db_files/rec","r").read())
    mdb = Mongod("mario", "12345")
    mdb.store_records(f)
    mdb.exit()
    del mdb