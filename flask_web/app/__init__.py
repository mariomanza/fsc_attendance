from flask import Flask
from app.backend.mongo_class import Mongod
from config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = Mongod('mario','12345')

from app import routes